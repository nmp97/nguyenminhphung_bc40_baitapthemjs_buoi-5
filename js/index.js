// Format Number
const formatNumber = new Intl.NumberFormat('vi-VN', {
  style: 'currency',
  currency: 'VND',
  currencyDisplay: 'code',
});

// EXCERSE 3
var ex3Btn = document.getElementById('ex3__btn');

ex3Btn.addEventListener('click', function () {
  const PERCENT_60 = 0.05;
  const PERCENT_120 = 0.1;
  const PERCENT_210 = 0.15;
  const PERCENT_384 = 0.2;
  const PERCENT_624 = 0.25;
  const PERCENT_960 = 0.3;
  const PERCENT_UP_960 = 0.35;

  let name = document.getElementById('ex3__name').value;
  let total = document.getElementById('ex3__total').value * 1;
  let person = document.getElementById('ex3__person').value * 1;
  let personIncomeTax = 0;

  let incomeTax = total - 4000000 - 1600000 * person;
  if (incomeTax >= 5600000) {
    if (incomeTax <= 60000000) {
      personIncomeTax += incomeTax * PERCENT_60;
    } else if (incomeTax <= 120000000) {
      personIncomeTax += incomeTax * PERCENT_120;
    } else if (incomeTax <= 210000000) {
      personIncomeTax += incomeTax * PERCENT_210;
    } else if (incomeTax <= 384000000) {
      personIncomeTax += incomeTax * PERCENT_384;
    } else if (incomeTax <= 624000000) {
      personIncomeTax += incomeTax * PERCENT_624;
    } else if (incomeTax <= 960000000) {
      personIncomeTax += incomeTax * PERCENT_960;
    } else if (incomeTax > 960000000) {
      personIncomeTax += incomeTax * PERCENT_UP_960;
    }
  } else {
    alert('Dữ liệu không hợp lệ');
  }

  document.getElementById(
    'ex3__result'
  ).innerHTML = `Họ và tên: ${name} - Tổng thuế TNCN: ${formatNumber.format(personIncomeTax)}`;
});

// EXERCISE 4
var ex4Btn = document.getElementById('ex4__btn');
var connect = document.getElementById('ex4__connect');
var option = document.getElementById('option');

const NHA_DAN = 'ND';
const DOANH_NGHIEP = 'DN';
const CONNECT_COST_10 = 7.5;
const CONNECT_COST_UP_10 = 15;

option.addEventListener('change', function () {
  if (option.value === DOANH_NGHIEP) {
    connect.style.display = 'block';
  } else if (option.value === NHA_DAN) {
    connect.style.display = 'none';
  }
});

ex4Btn.addEventListener('click', function () {
  let customerCode = document.getElementById('ex4__customer-code').value;
  let channelNumber = document.getElementById('ex4__channel').value * 1;
  let connectNumber = document.getElementById('ex4__connect').value * 1;
  var optionValue = document.getElementById('option').value;
  let cableCost = 0;

  if (optionValue === NHA_DAN) {
    cableCost += 4.5 + 20.5 + 7.5 * channelNumber;
  } else if (optionValue === DOANH_NGHIEP) {
    if (connectNumber <= 10) {
      cableCost += 15 + 75 + 50 * channelNumber;
    } else if (connectNumber > 10) {
      cableCost += 15 + 75 + 5 * (connectNumber - 10) + 50 * channelNumber;
    }
  }

  document.getElementById(
    'ex4__result'
  ).innerHTML = `Mã khách hàng: ${customerCode} - Tiền Cáp: ${cableCost}$`;
});
